<x-layout title="Criar nova Série">
    <form action="{{ route('series.store') }}" method="post">
        @csrf

        <div class="row mb-3">
            <div class="col-8">
                <label for="name" class="form-label">Nome da Série:</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
            </div>
            <div class="col-2">
                <label for="seasons_quantity" class="form-label">Número de Temporadas:</label>
                <input type="number " name="seasons_quantity" id="seasons_quantity" class="form-control" value="{{ old('seasons_quantity') }}">
            </div>
            <div class="col-2">
                <label for="episodes_per_season" class="form-label">Número de episódios:</label>
                <input type="number" name="episodes_per_season" id="episodes_per_season" class="form-control" value="{{ old('episodes_per_season') }}">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Criar Série</button>
    </form>
    <a href="{{ route('series.index') }}" class="btn btn-secondary mt-2">Listar todas as Séries</a>
</x-layout>
