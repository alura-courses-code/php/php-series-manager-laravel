<x-layout title="Lista de Séries" :success="$success">
    @auth
        <a class="btn btn-primary mb-2" href="{{ route('series.create') }}">Criar nova série</a>
    @endauth
    <ul class="list-group">
        @foreach ($seriesList as $series)
            <li class="list-group-item d-flex justify-content-between align-items-center">
                @auth<a href="{{ route('seasons.index', $series->id) }}">@endauth{{ $series->name }}@auth</a>@endauth
                @auth
                    <div class="d-flex gap-2">
                        <a href="{{ route('series.edit', $series->id) }}" class="btn btn-success btn-sm">Editar</a>
                        <form action="{{ route('series.destroy', $series->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm">
                                Deletar
                            </button>
                        </form>
                    </div>
                @endauth
            </li>
        @endforeach
    </ul>
</x-layout>
