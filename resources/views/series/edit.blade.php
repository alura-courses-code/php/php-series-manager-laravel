<x-layout title='Editar Série "{!! $series->name !!}"'>
    <form action="{{ route('series.update', $series->id) }}" method="post">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name" class="form-label">Nome da Série:</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
        </div>
        <button type="submit" class="btn btn-primary">Atualizar Série</button>
    </form>
    <a href="{{ route('series.index') }}" class="btn btn-secondary mt-2">Listar todas as Séries</a>
</x-layout>
